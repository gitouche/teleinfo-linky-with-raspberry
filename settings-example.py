#!/usr/bin/env python

# device port serie
SERIAL='/dev/ttyAMA0'

# Fichier de log
LOGFILE='/var/log/teleinfo-releve.log'

# Nombre de trames a sauter entre deux transmission des mesures (par défaut 0)
# SKIPPED_TRAMES = 0

# Configuration influxdb
influxdb = {
    'HOST': '127.0.0.1',
    'PORT': '8086',
    'USER': 'teleinfo',
#    'PASSWORD': 'password',
    'DB_NAME': 'teleinfo',
}
