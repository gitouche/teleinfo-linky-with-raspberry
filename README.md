# Téléinfo Linky avec un Raspberry Pi
Surveiller sa consommation électrique en temps réel avec un compteur Linky et un Raspberry 

Forké de https://github.com/SebastienReuiller/teleinfo-linky-with-raspberry  
* Traitement binaire du flux de données et mode STANDARD inspiré de https://github.com/babs/teleinfo2influx
* Ajout d'un fichier de configuration externe
* Ajout d'un délai optionnel entre la transmission de deux trames

## Installation
```
cd /opt 
git clone https://git.garbaye.fr/gitouche/teleinfo-linky-with-raspberry.git
```
## Configuration 
Créer et modifier le fichier de configuration :
```
cd /opt/teleinfo-linky-with-raspberry
cp settings-example.py settings.py
vi settings.py
```
Ajuster l'unit systemd si besoin :
```
vi teleinfo.service
```
Perréniser :
```
cp teleinfo.service /usr/lib/systemd/system/teleinfo.service
systemctl start teleinfo.service
systemctl enable teleinfo.service
```