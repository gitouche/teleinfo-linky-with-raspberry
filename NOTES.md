# TIC Téléinformation Client EDF
Notes diverses pour la compréhension.

## Informations générales TIC
<https://lucidar.me/fr/home-automation/linky-customer-tele-information/>

Sur un Linky, le "Mode historique" est actif par défaut. Il faut demander auprès de son fournisseur d'électricité le passage au "Mode standard" pour avoir plus d'informatoins. Le "Mode standard" est indispensable si on est redistributeur d'électricité (panneaux photovoltaïques)

## Montage électrique pour la TIC 
### schéma de base / montage historique
<https://www.magdiblog.fr/gpio/teleinfo-edf-suivi-conso-de-votre-compteur-electrique/>

### Suite d'articles

* Rôle des résistances : <http://hallard.me/demystifier-la-teleinfo/>
* Ajout transistor MOSFET N : <http://hallard.me/pitinfov12/>

### Forum :

* Tests sur les résistances : <https://community.ch2i.eu/topic/1053/point-resistances-ut%C3%A9l%C3%A9info-v2-et-linky-mode-standard>
* <https://community.ch2i.eu/assets/uploads/files/1667290928051-56e72161-3b0d-4f5e-b889-a0264b3122ba-image.png>

### Montage utilisé (testé)
```
R depuis la téléinfo 2 = 1k ohm
R borne 3 octocoupleur = 3.3k ohm
```

## Configuration port série sur un Raspberry PI

RPI / GPIO :
<https://www.magdiblog.fr/gpio/gpio-entree-en-matiere/>

### RPI1 et RPI2
Dans le fichier `/boot/cmdline.txt` :
- supprimer :
```
console=serial0,115200
```
- ajouter : 
```
enable_uart=1
```
### RPI3 et RPI4
Dans le fichier `/boot/config.txt`, ajouter :
```
dtoverlay=pi3-miniuart-bt
```
Dans le fichier `/boot/cmdline.txt`, supprimer :
```
console=serial0,115200
```

### Tous les RPI
+ rebooter

Configuration du port série :
```
stty -F /dev/ttyAMA0 1200 sane evenp parenb cs7 -crtscts
```

Version permanente : ajouter à la crontab
```
@reboot stty -F /dev/ttyAMA0 9600 sane evenp parenb cs7 -crtscts
```

Vérifier ce qui arrive sur le port série :
```
cat /dev/ttyAMA0
```

## Script pour traiter l'output série
<https://git.garbaye.fr/gitouche/teleinfo-linky-with-raspberry>
### Pré-requis pour le script (raspbian):
```
apt install python3-serial python3-influxdb
```